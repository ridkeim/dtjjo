/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtjjo;

/**
 *
 * @author ridkeim
 */
public class BlockBuilder{
    
    public final class Block{
        private final String text;
        private final int l_count;
        private final int c_count;
        private final int n_count;
        private final int other_count;
        private final int length;
        private Block(String text, int l_count, int c_count, int n_count, int other_count, int length){
            this.text = text;
            this.l_count = l_count;
            this.c_count = c_count;
            this.n_count = n_count;
            this.other_count = other_count;
            this.length = length;
        };

        public String getText() {
            return text;
        }

        public int getC_count() {
            return c_count;
        }

        public int getL_count() {
            return l_count;
        }

        public int getN_count() {
            return n_count;
        }

        public int getOther_count() {
            return other_count;
        }

        public int getLength() {
            return length;
        }

        @Override
        public String toString() {
            return text;
        }
        
    }
    
    private StringBuilder b;
    private int cyr_cnt;
    private int oth_cnt;
    private int ar_cnt;
    private int lat_cnt;
    private int length;
    private int max_lenght;
    private int min_lenght;
    private long all_length;
    
    public BlockBuilder() {
        b=new StringBuilder();
        max_lenght = 0;
        min_lenght = Integer.MAX_VALUE;
    }
    
    BlockBuilder appendCharacter(int codepoint){
        
        b.appendCodePoint(codepoint);
        if(isArabic(codepoint)){
            ar_cnt++;
        }else
        if(isCyrillic(codepoint)){
            cyr_cnt++;
        }else
        if(isLatin(codepoint)){
            lat_cnt++;
        }else{
            oth_cnt++;
        }
        length++;
        return this;
    };
    
    private boolean inRange(int cnt, int start, int stop){
        return cnt>=start && cnt<=stop; 
    }
    
    private boolean isCyrillic(int codepoint){
        return codepoint == 1025 || codepoint == 1105 || inRange(codepoint, 1072, 1103) || inRange(codepoint, 1040, 1071);
    };
    private boolean isArabic(int codepoint){
        return inRange(codepoint, 48, 57);
    };
    private boolean isLatin(int codepoint){
        return inRange(codepoint, 97, 122) || inRange(codepoint, 65, 90);
    };
    
    public void reset(){
        if(length>0){
            b.setLength(0);
            cyr_cnt =0;
            oth_cnt = 0;
            ar_cnt = 0;
            lat_cnt =0;
            length = 0;
        }
    }

    public int getLength() {
        return length;
    }

    public int getMax_lenght() {
        return max_lenght;
    }

    public int getMin_lenght() {
        return (min_lenght==Integer.MAX_VALUE)?max_lenght:min_lenght;
    }

    public long getAll_length() {
        return all_length;
    }
    
    
    
    public Block build(){
        Block res = new Block(b.toString(),lat_cnt,cyr_cnt,ar_cnt,oth_cnt,length);
        if(length>0){
            if(max_lenght<length){
                max_lenght=length;
            }
            if(min_lenght > length){
                min_lenght = length;
            }
            all_length+=length;
        }
        reset();
        return res;
    };
}

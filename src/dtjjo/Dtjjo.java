/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtjjo;

import com.sun.javafx.binding.StringFormatter;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author ridkeim
 */
public class Dtjjo {

    static String usage_msg = "usage: dtjjo.jar \'text\' \'o\' \'c\'\n\ttext - исходный текст\n\to - открывюащий символ\n\tc - закрывающий символ";
    static String error_msg = "аргумент \'%s\' должен быть символом\n";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String txt, os, cs;
        if (args.length == 3) {
            txt = args[0];
            os = args[1];
            cs = args[2];
        } else {
            System.out.println(usage_msg);
            return;
        }
        boolean invalid_os = os.codePointCount(0, os.length()) > 1;
        boolean invalid_cs = cs.codePointCount(0, cs.length()) > 1;
        boolean invalid_usage = invalid_os || invalid_cs;
        if (invalid_usage) {
            System.out.println(usage_msg);
            if (invalid_os) {
                System.out.printf(error_msg, os);
            }
            if (invalid_cs) {
                System.out.printf(error_msg, cs);
            }
            return;
        }
        perform(txt, os.codePointAt(0), cs.codePointAt(0));
    }

    static void perform(String txt, int oscp, int cscp) {
        //При одинаковых отделяющих символах, 
        //если выбрано true, отделяющий символ считается за закрываюхий и открывающий,
        //если false, отделяющий символ считается либо как закрывающий, либо как открывающий.
        boolean asOne = false;
        boolean isOpened = false;
        boolean isNeedClosing = asOne && (oscp == cscp);
        BlockBuilder bBuilder = new BlockBuilder();
        ArrayList<BlockBuilder.Block> li = new ArrayList<>();
        for (int i = 0; i < txt.length(); i++) {
            int cp = txt.codePointAt(i);
            if (isOpened) {
                if (cp == cscp) {
                    if (bBuilder.getLength() > 0) {
                        li.add(bBuilder.build());
                    }
                    isOpened = isNeedClosing;
                } else if (cp == oscp) {
                    bBuilder.reset();
                } else {
                    bBuilder.appendCharacter(cp);
                }
            } else if (cp == oscp) {
                isOpened = true;
            }
            i += Character.isSupplementaryCodePoint(cp) ? 1 : 0;
        }
        printResult(bBuilder, li);
    }
    
    static void printResult(BlockBuilder b,ArrayList<BlockBuilder.Block> blocks){
        SimpleDateFormat sdf = new SimpleDateFormat("_yyyyMMdd_hhmmss");
        File file = new File("result"+sdf.format(Calendar.getInstance().getTime())+".json"); 
        try (PrintWriter out = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file)))){
            out.write("{\n");
            out.write("\t\"blockCount\" : "+blocks.size()+",\n");
            double average = (blocks.size()>0)?b.getAll_length()/blocks.size():0;
            out.write("\t\"averageLength\" : "+StringFormatter.format(Locale.ENGLISH,"%.2f", average).get()+",\n");
            out.write("\t\"maxLength\" : "+b.getMax_lenght()+",\n");
            out.write("\t\"minLength\" : "+b.getMin_lenght()+",\n");
            if(blocks.size()>0){
                out.write("\t\"blocks\" : [\n");
                for (BlockBuilder.Block block : blocks) {
                    out.write("\t\t{\n");
                    out.write("\t\t\t\"text\" : \""+block.getText()+"\",\n");
                    out.write("\t\t\t\"textLength\" : "+block.getLength()+",\n");
                    out.write("\t\t\t\"latinCharactersCount\" : "+block.getL_count()+",\n");
                    out.write("\t\t\t\"cyrillicCharactersCount\" : "+block.getC_count()+",\n");
                    out.write("\t\t\t\"arabicNumeralsCount\" : "+block.getN_count()+",\n");
                    out.write("\t\t\t\"otherCharactersCount\" : "+block.getOther_count()+",\n");
                    out.write("\t\t},\n");
                }
                out.write("\t]\n");
            }
            out.write("}\n");
            out.flush();
            System.out.println("Результат в файле "+file.getAbsolutePath());
        } catch (FileNotFoundException ex) {
            if(!file.canWrite()){
                System.out.println("Не могу записать результат в файл : "+file.getAbsolutePath());
            }
        }
    }
}
